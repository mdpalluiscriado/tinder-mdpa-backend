// Imports
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const passport = require('passport')
const routes = require('./app/routes/routes')
const configDB = require('./app/config/database')

var app = express()
var port = process.env.PORT || 3000

// configuration ===============================================================

// Configure mongo
mongoose.Promise = global.Promise;
mongoose.connect(configDB.url, { useMongoClient: true });

// Configure app
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Configure passport
app.use(passport.initialize());
app.use(passport.session());

// Configure routes
routes(app);

// Start listening
app.listen(port);
console.log('Tinder API started on: ' + port);

module.exports = app