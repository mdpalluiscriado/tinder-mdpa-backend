//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require("mongoose");
const User = require('../app/models/user');

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

chai.use(chaiHttp);

describe('/users endpoint tests', () => {
    beforeEach((done) => {
        User.remove({}, (err) => {
            done();
        });
    });
    
    /*
    * Test the /GET route
    */
    describe('/GET users', () => {
        it('it should GET all the users in the test database', (done) => {
            chai.request(server)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

    /*
    * Test the /POST route
    */
    describe('/POST users', () => {
        it('it should POST a user new user and return it', (done) => {
            let user = {
                username: "Test user",
                password: "password",
                email: "test@email.com",
                userId: "ThisIsMyUserId"
            }
            chai.request(server)
                .post('/users')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.username.should.be.eql('Test user');
                    done();
                });
        });
    });
});

describe('/users/:userId endpoint tests', () => {
    beforeEach((done) => {
        User.remove({}, (err) => {
            done();
        });
    });
    /*
    * Test the /GET route
    */
    describe('/GET user', () => {
        it('it should GET the user with the userID of the parameter :userId', (done) => {
            chai.request(server)
                .get('/users/ThisIsMyUserId')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.userId.should.be.eql('ThisIsMyUserId');
                    done();
                });
        });
    });

    /*
     * Test the /PUT route
    */
    describe('/PUT user', () => {
        it('it should PUT the user with the userID of the parameter :userId and change its email field', (done) => {
            let user = {
                email: "test@emailChanged.com"
            }
            chai.request(server)
                .put('/users/ThisIsMyUserId')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.email.should.be.eql('test@emailChanged.com');
                    done();
                });
        });
    });
});

/*
* Test the /POST route
*/
describe('/POST users/:usersId', () => {
    it('it should POST a user new user and return it', (done) => {
        let user = {
            username: "Test user",
            password: "password",
            email: "test@email.com",
            userId: "this is my user id"
        }
        chai.request(server)
            .post('/users')
            .send(user)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.username.should.be.eql('Test user');
                done();
            });
    });
});
