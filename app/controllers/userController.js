// Controller for user

const mongoose = require('mongoose')
const User = mongoose.model('User')
const Conversation = mongoose.model('Conversation')

// Public functions ===============================================================

// ===============================================================
// GET all users
exports.list_all_users = function (req, res) {
  User.find({},
    function (err, user) {
      if (err) res.send(err);
      res.json(user);
    });
};

// ===============================================================
// Create a new user
exports.create_a_user = function (req, res) {
  var new_user = new User(req.body);
  new_user.password = new_user.generateHash(req.body.password);
  new_user.userId = Math.random().toString(36).substr(2, 15);   // TODO: change for UUID
  new_user.save(function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

// ===============================================================
// Update a user by userId
exports.update_a_user = function (req, res) {
  User.findOneAndUpdate({ userId: req.params.userId }, req.body, { new: true },
    function (err, user) {
      if (err) res.send(err);
      res.json(user);
    });
};

// ===============================================================
// Add userSeen value
exports.add_user_seen = function (req, res) {
  var myUserId = req.params.userId
  var userSeen = { userSeenId: req.body.userSeenId, isLiked: req.body.isLiked }

  // Add user seen object
  User.updateOne({ userId: myUserId }, { $push: { usersSeen: { userSeenId: userSeen.userSeenId, isLiked: userSeen.isLiked } } },
    function (err) {
      if (err) res.send(err);

      // Check if it's a match
      chckIfMatch(myUserId, userSeen,
        function (err, match) {
          if (err) res.send(err);

          // Adds userId to matches array
          if (match) { addMatchForUsers(myUserId, userSeen.userSeenId) }

          res.json({ message: 'User seen added correctly', match: match })
        });
    });
};

// ===============================================================
// Get matches
exports.get_matches = function (req, res) {

  User.findOne({ userId: req.params.userId },
    function (err, user) {
      if (err) res.send(err);

    // Get matches from user
    User.find({ userId: { $in: user.matches } }, 
      function(err, matches) {
        if (err) res.send(err);

        res.json(matches);
    });
  });
};

// ===============================================================
// Get Conversations
exports.get_conversations = function (req, res) {

  User.findOne({ userId: req.params.userId },
    function (err, user) {
      if (err) res.send(err);

    // Get conversations from user
    Conversation.find({ conversationId: { $in: user.conversations } }, 
      function(err, conversations) {
        if (err) res.send(err);

        res.json(conversations);
    });
  });
};

// Private functions ===============================================================

function chckIfMatch(myUserId, userSeen, callback) {
  if (userSeen.isLiked) {
    User.findOne({ userId: userSeen.userSeenId },
      function (err, user) {
        if (err) callback(true, null)

        callback(null, isMyUserLiked(myUserId, user.usersSeen))
      })
  } else {
    // if my isLiked value is false, it will never be a match
    callback(null, false)
  }
}

function isMyUserLiked(userSeenId, usersSeen) {
  var isMyUserLiked = null
  for (var i = 0; i < usersSeen.length; i++) {
    if (usersSeen[i].userSeenId === userSeenId) {
      isMyUserLiked = usersSeen[i].isLiked
    }
  }
  return isMyUserLiked
}

function addMatchForUsers(user1Id, user2Id) {
  User.updateOne({ userId: user1Id }, { $push: { matches: user2Id } }, 
    function (err) {
      if (err) console.log('error: ', err);
    });

  User.updateOne({ userId: user2Id }, { $push: { matches: user1Id } }, 
    function (err) {
      if (err) console.log('error: ', err);
    });

  // TODO: send push notification for new match for user2
}