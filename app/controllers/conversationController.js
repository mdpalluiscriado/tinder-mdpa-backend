// Controller for conversation

const mongoose = require('mongoose')
const Conversation = mongoose.model('Conversation')
const User = mongoose.model('User')
const Message = mongoose.model('Message')

// Public functions ===============================================================

// ===============================================================
// Create a new conversation
exports.create_a_conversation = function (req, res) {
  var new_conversation = new Conversation();
  new_conversation.conversationId = Math.random().toString(36).substr(2, 15);   // TODO: change for UUID
  new_conversation.users = req.body.users
  new_conversation.messages = req.body.message

  new_conversation.save(function (err, conversation) {
    if (err) res.send(err);

    addConversationForUsers(conversation.conversationId, conversation.users)

    res.json(conversation.conversationId);
  });
};

// ===============================================================
// Add conversation value
exports.add_message = function (req, res) {
  var myConversationId = req.params.conversationId
  var new_message = new Message(req.body.message);
  new_message.messageId = Math.random().toString(36).substr(2, 15);   // TODO: change for UUID

  // Add message object
  Conversation.updateOne({ conversationId: myConversationId }, { $push: { messages: new_message } },
    function (err) {
      if (err) res.send(err);

      res.json(myConversationId);
    });
};

// ===============================================================
// Get messages
exports.get_messages = function (req, res) {

  // Get messages value
  Conversation.findOne({ conversationId: req.params.conversationId },
    function (err, conversation) {
      if (err) res.send(err);

      res.json(conversation.messages);
    });
};


// Private functions ===============================================================

function addConversationForUsers(conversationId, users) {
  for (var index = 0; index < users.length; index++) {
    User.updateOne({ userId: users[index].userId }, { $push: { conversations: conversationId } },
      function (err) {
        if (err) console.log('error: ', err);
      });
  }
}
