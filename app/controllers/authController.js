// Controller for authentication
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const FacebookStrategy = require('passport-facebook').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const User = require('../models/user')
const configAuth = require('../config/auth')
const jwt = require('jsonwebtoken');


const jwtOptions = {  
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: configAuth.jwtSecret
};

// =========================================================================
// passport session setup ==================================================
// =========================================================================

// used to serialize the user for the session
passport.serializeUser(function (user, done) {
  done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});


// =========================================================================
// JWT AUTH ================================================================
// =========================================================================

passport.use('jwt', new JwtStrategy(jwtOptions,
  function (payload, callback) {
    console.log('JWT authentification recieved', payload)

    User.findOne({userId: payload.userId}, function (err, user) {
      if (err) { return callback(err, false); }

      if (user) {  callback(null, user) } 
      else { callback(null, false) }
    })
  }
));


// =========================================================================
// LOCAL LOGIN =============================================================
// =========================================================================

passport.use('local-login', new LocalStrategy(
  function (username, password, callback) {

    User.findOne({ username: username }, function (err, user) {
      if (err) { return callback(err); }

      // No user found with that username
      if (!user) {
        return callback(null, false, { message: 'Incorrect username.' });
      }

      // Invalid password
      if (!user.validPassword(password)) {
        return callback(null, false, { message: 'Wrong password.' });
      }

      // Success
      return callback(null, user);
    });
  }
));

// =========================================================================
// LOCAL SIGNUP ============================================================
// =========================================================================

passport.use('local-signup', new LocalStrategy(
  function (username, password, callback) {
    User.findOne({ username: username }, function (err, existingUser) {
      if (err) return callback(err);

      // check to see if there's already a user with that email
      if (existingUser) {
        return callback(null, false, { message: 'User trying to signup with existing username' });
      } else {
        //  If we're here, there is no user in DB with that username, create a new User in DB.
        
        var newUser = new User()
        newUser.username = username
        newUser.password = newUser.generateHash(password)
        newUser.userId = Math.random().toString(36).substr(2, 15)

        // TODO: REMOVE MOCK DATA!
        newUser.email = 'lluis.criado@gmail.com'
        newUser.profilePicLink = 'https:media-public.fcbarcelona.com/20157/0/document_thumbnail/20197/174/102/254/50226862/1.0-7/50226862.jpg?t=1500558216000'
        newUser.description = 'I am a football player'
        newUser.currentJob = 'Football player'
        newUser.university = 'FC Barcelona'
        newUser.isMale = true
        newUser.ageRangePreference.from = 18
        newUser.ageRangePreference.to = 30        
        
        newUser.save(function (err) {
          if (err) throw err;

          return callback(null, newUser);
        });
      }
    });
  }));


// =========================================================================
// FACEBOOK ================================================================
// =========================================================================

passport.use(new FacebookStrategy({
  clientID: configAuth.facebookAuth.clientID,
  clientSecret: configAuth.facebookAuth.clientSecret,
  callbackURL: configAuth.facebookAuth.callbackURL
},
  function (token, refreshToken, profile, callback) {
    console.log('Facebook Login/Signup attempt')

    // find the user in the database based on the facebook id
    User.findOne({ facebookId: profile.id }, function (err, user) {
      if (err) return callback(err);

      // if the user is found, then this is a login
      if (user) {
        return callback(null, user);
      } else {

        // if there is no user found with that facebook id, create one in DB
        var newUser = new User();

        // set all of the facebook information in our user model
        newUser.facebookId = profile.id;
        newUser.facebookToken = token;
        newUser.username = profile.name.givenName + ' ' + profile.name.familyName;
        newUser.email = profile.emails[0].value;

        // save our user to the database
        newUser.save(function (err) {
          if (err) throw err;

          // Success
          return callback(null, newUser);
        });
      }
    });
  }));


// =========================================================================
// EXPORTS =================================================================
// =========================================================================

exports.getToken = function (req, res) {
  var token = jwt.sign(req.user.userId, configAuth.jwtSecret);
  res.json({ success: true, token: 'JWT ' + token, user: req.user });
}

exports.isCallAuthenticated = passport.authenticate('jwt', { session: false});
exports.authenticateLogin = passport.authenticate('local-login', { session: false});
exports.authenticateSignup = passport.authenticate('local-signup', { session: false});
exports.authenticateFacebook = passport.authenticate('facebook', { session: false});