// User model

const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const ConversationSchema = require('./conversation')

// User Schema ================================================================================

var UserSchema = mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: String,
  profilePicLink: String,
  email: String,
  userId: String,
  facebookId: String,
  facebookToken: String,
  isMale: Boolean,
  currentJob: String,
  description: String,
  university: String,
  favouriteSong: String,
  usersSeen: [{ userSeenId: String, isLiked: Boolean }],
  lastLocation: { latitude: Number, longitude: Number },
  ageRangePreference: { from: Number, to: Number },
  conversations: [String], // Array of conversations (conversation ids)
  matches: [String] // Array of matched users (user ids)
})

// User methods ================================================================================

// generating a hash
UserSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);