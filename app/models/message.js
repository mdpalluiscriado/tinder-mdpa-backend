// Message model

const mongoose = require('mongoose');

// Message Schema ============================================================================

var MessageSchema = mongoose.Schema({
    messageId: String,
    fromId: String,
    toId: String,
    content: String,
    timestamp: String,
    isRead: Boolean,
    type: String
})

// Message methods ============================================================================

module.exports = mongoose.model('Message', MessageSchema);