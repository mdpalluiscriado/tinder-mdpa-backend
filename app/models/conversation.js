// Conversation model

const mongoose = require('mongoose');
const Message = require('./message')

// Conversation Schema ============================================================================

var ConversationSchema = mongoose.Schema({
    conversationId: String,
    users: [
        {
        userId: String,
        username: String,
        profilePicLink: String
    }
    ],
    messages: [Message.schema]
})

// Conversation methods ============================================================================


module.exports = mongoose.model('Conversation', ConversationSchema);