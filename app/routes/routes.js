// Routes for authentication

const authController = require('../controllers/authController');
const userController = require('../controllers/userController');
const conversationController = require('../controllers/conversationController');

module.exports = function (app) {

  // Login And Signup routes ====================================================

  // Process Login requests
  app.route('/login')
    .post(authController.authenticateLogin, authController.getToken)

  // Process Signup requests
  app.route('/signup')
    .post(authController.authenticateSignup, authController.getToken)

  // Process Facebook requests
  app.route('/facebook')
    .post(authController.authenticateFacebook, authController.getToken)


  // Users routes ===============================================================

  // Endpoint getting and adding users
  app.route('/users')
    .get(userController.list_all_users)
    .post(userController.create_a_user)    

  // Process user requests
  app.route('/users/:userId')
    .post(userController.update_a_user)     

  // Process liked / disliked user
  app.route('/users/:userId/usersSeen')
    .post(userController.add_user_seen)    

  // Process get matches request
  app.route('/users/:userId/matches')
    .get(userController.get_matches)       

  // Process get matches request
  app.route('/users/:userId/conversations')
    .get(userController.get_conversations)  

  
  // Conversation routes ==========================================================

  // Endpoint getting and adding conversations
  app.route('/conversations')
    .post(conversationController.create_a_conversation)

  // Process add messages to conversation
  app.route('/conversations/:conversationId/messages')
    .post(conversationController.add_message)              
    .get(conversationController.get_messages)              
};